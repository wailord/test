```mermaid
gantt 
    axisFormat  %S
    section EDD
    1           :a1, 00, 10s
    2           :a2, after a1 , 1s
    3           :a3, after a2 , 1s
    4           :a4, after a3 , 1s
    5           :a5, after a4 , 1s
    section Optimal
    2           :b2, 00, 1s
    3           :b3, after b2 , 1s
    4           :b4, after b3 , 1s
    5           :b5, after b4 , 1s
    1           :b1, after b5, 10s
```